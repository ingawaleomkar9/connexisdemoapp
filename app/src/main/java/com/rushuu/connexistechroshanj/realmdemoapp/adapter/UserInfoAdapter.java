package com.rushuu.connexistechroshanj.realmdemoapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rushuu.connexistechroshanj.realmdemoapp.R;
import com.rushuu.connexistechroshanj.realmdemoapp.model.MasterResponse;

import java.util.List;

public class UserInfoAdapter extends RecyclerView.Adapter<UserInfoAdapter.UserViewHolder> {

    private Context context;
    private List<MasterResponse> userDetailsList;

    public UserInfoAdapter(Context context, List<MasterResponse> userDetailsList) {
        this.context = context;
        this.userDetailsList = userDetailsList;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_user_details, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {

        holder.textUserName.setText(userDetailsList.get(position).getName());
        holder.textEmailId.setText(userDetailsList.get(position).getEmail());
        //holder.textAddress.setText(movies.get(position).getOverview());
        holder.textPhone.setText(userDetailsList.get(position).getPhone());

    }

    @Override
    public int getItemCount() {
        return userDetailsList.size();
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {

        private TextView textUserName, textEmailId, textAddress, textPhone;

        public UserViewHolder(View itemView) {
            super(itemView);
            textUserName = itemView.findViewById(R.id.text_name);
            textEmailId = itemView.findViewById(R.id.text_email);
            textAddress = itemView.findViewById(R.id.text_address);
            textPhone = itemView.findViewById(R.id.text_phone);
        }
    }
}
