package com.rushuu.connexistechroshanj.realmdemoapp.service;

import com.rushuu.connexistechroshanj.realmdemoapp.model.MasterResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiListener {

    @GET("users")
    Call<List<MasterResponse>> getUserDetails();
}
