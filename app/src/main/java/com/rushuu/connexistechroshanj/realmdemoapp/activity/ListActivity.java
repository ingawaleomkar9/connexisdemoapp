package com.rushuu.connexistechroshanj.realmdemoapp.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.rushuu.connexistechroshanj.realmdemoapp.R;
import com.rushuu.connexistechroshanj.realmdemoapp.adapter.UserInfoAdapter;
import com.rushuu.connexistechroshanj.realmdemoapp.model.MasterResponse;
import com.rushuu.connexistechroshanj.realmdemoapp.service.ApiClient;
import com.rushuu.connexistechroshanj.realmdemoapp.service.ApiListener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListActivity extends AppCompatActivity {

    private static final String TAG = "ListActivity";
    private RecyclerView recyclerView;
    private ProgressDialog progressDialog;
    private Realm realm;
    private List<MasterResponse> responses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);


        initViews();
        makeApiRequest();
    }

    /**
     * Function to initialize view we are going to use
     */
    private void initViews() {
        recyclerView = findViewById(R.id.recycler_user_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Getting your Data");
        progressDialog.setCancelable(false);
        // Create the Realm instance

        realm = Realm.getDefaultInstance();
        responses = new ArrayList<>();
    }


    /**
     * Function to make API call to get user data and pass it to Adapter
     */
    private void makeApiRequest() {

        progressDialog.show();
        ApiListener apiListener =
                ApiClient.getClient().create(ApiListener.class);

        Call<List<MasterResponse>> call = apiListener.getUserDetails();
        call.enqueue(new Callback<List<MasterResponse>>() {
            @Override
            public void onResponse(Call<List<MasterResponse>> call, Response<List<MasterResponse>> response) {
                progressDialog.dismiss();
                List<MasterResponse> masterResponses = response.body();

                /**
                 * commented data getting from server. Based on network condition have to show the data. For now I am getting data from server
                 * and storing into db and that stored data I am showing
                 */

                //recyclerView.setAdapter(new UserInfoAdapter(getApplicationContext(), masterResponses));
                Log.e(TAG, "Response--> " + masterResponses.toString());

                //adding data to Realm
                addDataToRealm(masterResponses);

                //reading data from db and passing to adapter
                RealmResults results = realm.where(MasterResponse.class).findAll();
                List<MasterResponse> arrayListOfUnmanagedObjects = realm.copyFromRealm(results);
                recyclerView.setAdapter(new UserInfoAdapter(getApplicationContext(),arrayListOfUnmanagedObjects));
            }

            @Override
            public void onFailure(Call<List<MasterResponse>> call, Throwable t) {
                // Log error here since request failed
                progressDialog.dismiss();
                Log.e(TAG, t.toString());
            }
        });

    }


    private void addDataToRealm(List<MasterResponse> model) {
            try(Realm realm = Realm.getDefaultInstance()) {
                realm.executeTransaction(realm1 -> {
                    RealmList<MasterResponse> _newsList = new RealmList<>();
                    _newsList.addAll(model);
                    realm1.insert(_newsList); // <-- insert unmanaged to Realm
                });


        } catch (Exception e) {
            Log.e(TAG, "Exception--> " + e.toString());
        }
    }

}
