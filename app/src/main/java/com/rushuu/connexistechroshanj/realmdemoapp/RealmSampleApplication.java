package com.rushuu.connexistechroshanj.realmdemoapp;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class RealmSampleApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);

    }
}
